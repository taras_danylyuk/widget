import React from 'react';
import {render} from 'react-dom';

import Widget from './Widget.jsx';
import '../../styles/main.scss';

const item = {
    title: 'Nike Air Max',
    price: 155,
    colors: ['#2faae2', '#37c5ff'],
    imageUrl: '../Sneaker.png',
    size: 'm'
}

class App extends React.Component {
    render () {
        return ( <Widget item={item} /> );
    }
}

render(<App/>, document.getElementById('app'));