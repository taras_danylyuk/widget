import React from 'react';

export default class Widget extends React.Component {

    constructor(props) {
        super(props);
        this.state = {amount : 2};
        this._onChange = this._onChange.bind(this);
    }

    _onChange(event) {
        this.setState({amount: event.target.value});
    }

    _onKeyPress(event) {
        if (event.which !== 8 && event.which !== 0 && event.which < 48 || event.which > 57) event.preventDefault();
    }

    render() {
        const {item} = this.props;

        return (
            <div className="widget-wrapper">
                <div className="shopping-cart-block">
                    <div className="cart-header">
                        <span className="cart-title">Shopping Cart</span>
                        <span className="cart-close">&times;</span>
                    </div>

                    <div className="cart-content">
                        <div className="left-col col">
                            <div className="item-background" />
                            <img className="item-image" src={item.imageUrl} />
                        </div>

                        <div className="right-col col">
                            <span className="item-title">{item.title}</span>
                            <span className="item-price">${item.price.toFixed(2)}</span>
                            <div className="item-control">
                                <input type="number"
                                       className="item-amount"
                                       value={this.state.amount}
                                       onChange={this._onChange}
                                       onKeyPress={this._onKeyPress}
                                />
                                <div className="item-additional">
                                    <span className="item-additional-text">Size: {item.size.toUpperCase()}</span>
                                    <span className="item-additional-text">Color: <ItemColors colors={item.colors} /></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="cart-footer">
                        <button className="cart-checkout">Checkout</button>

                        <div className="cart-total">
                            <p className="total-title">Total Price</p>
                            <p className="total-text">${(item.price * this.state.amount).toFixed(2)}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const ItemColors = ({colors}) => {
    return (
        <div className="colors-wrapper">
            { colors.map(color => <div key={color} style={{height: '35px', width: `${25 / colors.length}px`, background: color, transform: 'rotate(15deg)'}} />) }
        </div>
    );
};